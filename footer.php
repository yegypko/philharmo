<footer>
    <h4 class="tittleBlock text-center"><?php the_field('title_footer', 2); ?></h4>
    <!-- <div class="owl-carousel owl-theme logoList actSliderFooter">
                    <div class="item">
                        <a href=""><img src="" alt="logo1" class="logo1"></a>
                    </div>
                    <div class="item">
                        <a href="#"><img src="<?php echo bloginfo ('template_url'); ?>/images/logo/logo2.png" alt="logo2" class="logo2"></a>
                    </div>
                    <div class="item">
                        <a href="#"><img src="<?php echo bloginfo ('template_url'); ?>/images/logo/logo3.png" alt="logo3" class="logo3"></a>
                    </div>
                    <div class="item">
                        <a href="#"><img src="<?php echo bloginfo ('template_url'); ?>/images/logo/logo4.png" alt="logo4" class="logo4"></a>
                    </div>
                    <div class="item">
                        <a href="#"><img src="<?php echo bloginfo ('template_url'); ?>/images/logo/logo5.png" alt="logo5" class="logo5"></a>
                    </div>
                    <div class="item">
                        <a href="#"><img src="<?php echo bloginfo ('template_url'); ?>/images/logo/logo7.png" alt="logo7" class="logo6"></a>
                    </div>
                    <div class="item">
                        <a href="#"><img src="<?php echo bloginfo ('template_url'); ?>/images/logo/logo8.png" alt="logo8" class="logo7"></a>
                    </div>    
    </div> -->
   
    <ul class="logoList">
        <?php
            $partners = get_field('partners_logos', 2);
            if( $partners ): ?>
                <li><a href="<?php echo $partners['link_1']; ?>"><img src="<?php echo $partners['logo_1']; ?>" alt="logo1"></a></li>
                <li><a href="#"><img src="<?php echo bloginfo ('template_url'); ?>/images/logo/logo2.png" alt="logo2"></a></li>
                <li><a href="#"><img src="<?php echo bloginfo ('template_url'); ?>/images/logo/logo3.png" alt="logo3"></a></li>
                <li><a href="#"><img src="<?php echo bloginfo ('template_url'); ?>/images/logo/logo4.png" alt="logo4"></a></li>
                <li><a href="#"><img src="<?php echo bloginfo ('template_url'); ?>/images/logo/logo5.png" alt="logo5"></a></li>
                <li><a href="#"><img src="<?php echo bloginfo ('template_url'); ?>/images/logo/logo7.png" alt="logo7"></a></li>
                <li><a href="#"><img src="<?php echo bloginfo ('template_url'); ?>/images/logo/logo8.png" alt="logo8"></a></li>

        <?php endif; ?>
    </ul>
    <div class="back-to-top">
        <img src="<?php echo bloginfo ('template_url'); ?>/images/upArroy.png" alt="up">
    </div>
    
        <?php the_field('contact_information', 2); ?>
    
    
    <ul class="footerListSoc">
        <?php
            $social = get_field('social', 2);
            if( $social ): ?>
                <li><a href="<?php echo $social['facebook']; ?>"><img src="<?php echo bloginfo ('template_url'); ?>/images/fb.png" alt="facebook"></a></li>
                <li><a href="<?php echo $social['twiter']; ?>"><img src="<?php echo bloginfo ('template_url'); ?>/images/tw.png" alt="twiter"></a></li>
                <li><a href="<?php echo $social['instagram']; ?>"><img src="<?php echo bloginfo ('template_url'); ?>/images/ins.png" alt="instagram"></a></li>
                <li><a href="<?php echo $social['youtube']; ?>"><img src="<?php echo bloginfo ('template_url'); ?>/images/youtube.png" alt="youtube"></a></li>
            <?php endif; ?>
        <li><a href="#"><img src="<?php echo bloginfo ('template_url'); ?>/images/search.png" alt="search"></a></li>
    </ul>

        <?php the_field('design', 2); ?>
    

</footer>
<?php
wp_footer();
?>
</body>
</html>