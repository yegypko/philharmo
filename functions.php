<?php
    add_action('wp_enqueue_scripts', 'philharmo_scripts');

    function philharmo_scripts() {
        wp_enqueue_style( 'philharmo-style-bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css' );
        wp_enqueue_style( 'philharmo-style-font-awesome', get_template_directory_uri() . '/css/font-awesome.min.css' );
        wp_enqueue_style( 'philharmo-style-owl-carousel', get_template_directory_uri() . '/css/owl.carousel.min.css' );
        wp_enqueue_style( 'philharmo-style-animate', get_template_directory_uri() . '/css/animate.css' );
        wp_enqueue_style( 'philharmo-style', get_stylesheet_uri() );

        wp_enqueue_script( 'philharmo-scripts-jquery', get_template_directory_uri() . '/js/jquery.min.js');
        wp_enqueue_script( 'philharmo-scripts-bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js');
        wp_enqueue_script( 'philharmo-scripts-owl-carousel', get_template_directory_uri() . '/js/owl.carousel.min.js');
        wp_enqueue_script( 'philharmo-scripts-jquery-h5validate', get_template_directory_uri() . '/js/jquery.h5validate.js');
        wp_enqueue_script( 'philharmo-scripts', get_template_directory_uri() . '/js/main.js', array(), null, true);
    };

    add_theme_support( 'post-thumbnails' );
    add_theme_support( 'custom-logo' );

?>
