<?php
get_header();
?>
<section class="articleSpaceBlock container">
<?php 
 ;
				/*
				 * The WordPress Query class.
				 *
				 * @link http://codex.wordpress.org/Function_Reference/WP_Query
				 */
				$frontpagePosts = new WP_Query (array(
					// Post & Page Parameters
                    'posts_per_page'=> -1,
                    'order'         => 'ASC',
				));
			
			while ( $frontpagePosts->have_posts()) {
				$frontpagePosts->the_post(); ?>
                <div class="articleSpace">
                    <div class="imgBlock">
                        <img src="<?php echo the_post_thumbnail_url(); ?>" alt="articlePic">
                    </div>

                    <div class="contentText">
                        <h5 class="titleArticle"><?php the_title();?></h5>
                        <p><?php the_field('description_post'); ?></p>
                        <a href="<?php the_permalink();?>" class="moreInfo">MORE INFO</a>
                        <a href="<?php the_field('booking_link'); ?>" class="bookNow">BOOK NOW</a>
                    </div>
                </div>

    <?php } wp_reset_postdata();
            
    ?>
</section>

<?php
get_footer();
?>