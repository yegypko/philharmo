<?php
get_header();
?>
<section class="sliderBlock">
    <div class="owl-carousel owl-theme actSlider">

        <?php 
				/*
				 * The WordPress Query class.
				 *
				 * @link http://codex.wordpress.org/Function_Reference/WP_Query
				 */
				$frontpagePostSlider = new WP_Query (array(
					// Post & Page Parameters
                    'posts_per_page'=> 4,
                    'category_name' => 'uncategorized',
                    'order'         => 'DESC',
				));
			
			while ( $frontpagePostSlider->have_posts()) {
				$frontpagePostSlider->the_post(); ?>


            <div class="item mainSlider1">
                <div class="sliderStyle slider" style="background-image: url('<?php the_field('background_image_post'); ?>')">
                    <div class="containerSlider">
                        <div class="blockNow">
                            <h3><?php the_title();?></h3>
                            <p class="textSlider">
                            <?php the_field('text_for_slider'); ?>
                            </p>
                            <a href="<?php the_field('booking_link'); ?>" class="nowLink">BOOK NOW</a>
                        </div>
                    </div>
                </div>
            </div>

        <?php } wp_reset_postdata();
            
        ?>


    </div>
</section>

<section class="articleSpaceBlock container">




    <h4 class="tittleBlock padleft"><?php the_field('title_1'); ?></h4>

    <?php 
				/*
				 * The WordPress Query class.
				 *
				 * @link http://codex.wordpress.org/Function_Reference/WP_Query
				 */
				$frontpagePosts = new WP_Query (array(
					// Post & Page Parameters
                    'posts_per_page'=> 6,
                    'category_name' => 'uncategorized',
                    'order'         => 'ASC',
				));
			
			while ( $frontpagePosts->have_posts()) {
				$frontpagePosts->the_post(); ?>

                <div class="articleSpace">
                    <div class="imgBlock">
                        <img src="<?php echo the_post_thumbnail_url(); ?>" alt="articlePic">
                    </div>

                    <div class="contentText">
                        <h5 class="titleArticle"><?php the_title();?></h5>
                        <p><?php the_field('description_post'); ?></p>
                        <a href="<?php the_permalink();?>" class="moreInfo">MORE INFO</a>
                        <a href="<?php the_field('booking_link'); ?>" class="bookNow">BOOK NOW</a>
                    </div>
                </div>

    <?php } wp_reset_postdata();
            
    ?>


    <h4 class="tittleBlock padleft"><?php the_field('title_2'); ?></h4>
    
    <?php 
				/*
				 * The WordPress Query class.
				 *
				 * @link http://codex.wordpress.org/Function_Reference/WP_Query
				 */
				$frontpagePostsSing = new WP_Query (array(
					// Post & Page Parameters
                    'posts_per_page'=> 1,
                    'category_name' => 'sing-with-us',
                    'order'         => 'ASC',
				));
			
			while ( $frontpagePostsSing->have_posts()) {
				$frontpagePostsSing->the_post(); ?>

                <div class="articleSpace">
                    <div class="imgBlock">
                        <img src="<?php echo the_post_thumbnail_url(); ?>" alt="articlePic">
                    </div>

                    <div class="contentText">
                        <h5 class="titleArticle"><?php the_title();?></h5>
                        <p><?php the_field('description_post'); ?></p>
                        <a href="<?php the_permalink();?>" class="moreInfo">MORE INFO</a>
                        <a href="<?php the_field('booking_link'); ?>" class="bookNow">BOOK NOW</a>
                    </div>
                </div>

    <?php } wp_reset_postdata();
            
    ?>
    
</section>

<section class="youtube">
    <h4 class="tittleBlock padleft"><?php the_field('title_3'); ?></h4>

    <?php 
				/*
				 * The WordPress Query class.
				 *
				 * @link http://codex.wordpress.org/Function_Reference/WP_Query
				 */
				$frontpagePostsMedia = new WP_Query (array(
					// Post & Page Parameters
                    'posts_per_page'=> 1,
                    'category_name' => 'media',
                    'order'         => 'ASC',
				));
			
			while ( $frontpagePostsMedia->have_posts()) {
				$frontpagePostsMedia->the_post(); ?>



    <div class="video_container" data-youtube-src="<?php the_field('youtube_link'); ?>">
        <buttom class="play_button">
            <img src="<?php echo bloginfo ('template_url'); ?>/images/page/buttonStart.png" alt="start Play">
        </buttom>
        <div class="poster" >
            <img src="<?php echo the_post_thumbnail_url(); ?>" alt="videoYoutube">
        </div>
    </div>
    
    <?php } wp_reset_postdata();
            
    ?>

</section>

<section class="ourEvent container">
    <div class="row">
        <div class="col-md-4">
            <h4 class="tittleBlock"><?php the_field('title_4'); ?></h4>
            <a href="<?php the_field('link_page'); ?>">
                <img src="<?php the_field('thumbnail_page_1'); ?>" alt="fest">
            </a>
        </div>
        <div class="col-md-4">
            <h4 class="tittleBlock"><?php the_field('title_5'); ?></h4>
            <a href="<?php the_field('link_page_2'); ?>">
                <img src="<?php the_field('thumbnail_page_2'); ?>" alt="fest">
            </a>
        </div>
        <div class="col-md-4">
            <a href="<?php the_field('link_page_2'); ?>">
                <div class="concertBlockText">
                <?php the_field('preview_text'); ?>
                </div>
            </a>
        </div>
    </div>
</section>

<section class="subscribe">
    <h4 class="tittleBlock text-center">SUBSCRIBE.</h4>
    <div class="formEmail">
        <?php echo do_shortcode('[contact-form-7 id="232" title="SUBSCRIBE"]') ?>
    </div>
</section>

<section class="instagramBlock">
    <h4 class="tittleBlock text-center"><?php the_field('title_6'); ?></h4>
    <div class="wiget">
        <img src="<?php the_field('thumbnail_page_3'); ?>">
    </div>
</section>

<section class="socBlock">
    <ul class="socList">
        <?php
            $social = get_field('social');
            if( $social ): ?>
                <li>
                    <a href="<?php echo $social['facebook']; ?>">
                        <img src="<?php echo bloginfo ('template_url'); ?>/images/bigSocIcon/fb.png" alt="facebook">
                    </a>
                </li>
                <li>
                    <a href="<?php echo $social['twiter']; ?>">
                        <img src="<?php echo bloginfo ('template_url'); ?>/images/bigSocIcon/tw.png" alt="twiter">
                    </a>
                </li>
                <li>
                    <a href="<?php echo $social['instagram']; ?>">
                        <img src="<?php echo bloginfo ('template_url'); ?>/images/bigSocIcon/inst.png" alt="instagram">
                    </a>
                </li>
                <li>
                    <a href="<?php echo $social['youtube']; ?>">
                        <img src="<?php echo bloginfo ('template_url'); ?>/images/bigSocIcon/youtube.png" alt="youtube">
                    </a>
                </li>
            <?php endif; ?>
    </ul>
</section>
<?php
get_footer();
?>