<?php
get_header(); 
?>

	<div class="container margin-bottom">
		<div class="row">
			<h2 class="titleArticle single-post-title">
				<a href="<?php echo get_home_url();?>">
					Oops! That page can&rsquo;t be found.
				</a>
			</h2>
		</div>

	</div>

<?php
get_footer();
