<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title><?php bloginfo('name');?></title>
    <meta name="description" content="description">
    <meta name="keywords" content="keywords">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">

    <?php
    wp_head();
    ?>

</head>
<body>
<header class="container">
    <div class="row">
        <div class="col-md-7 col-sm-7">
            <ul class="logoLine">
                <li>
                   <a href="<?php echo get_home_url(); ?>" class="logoPage">
                        <img src="<?php 

                            $custom_logo__url = wp_get_attachment_image_src( get_theme_mod( 'custom_logo' ), 'full' ); 
                            echo $custom_logo__url[0];

                        ?>" alt="logo">
                    </a>
                    
                    
                </li>
                <li>
                    <a href="<?php the_field('link_1', 2); ?>">
                        <img src="<?php the_field('logo_1', 2); ?>" alt="season">
                    </a>
                </li>
                <li>
                    <a href="<?php the_field('link_2', 2); ?>">
                        <img src="<?php the_field('logo_2', 2); ?>" alt="season">
                    </a>
                </li>
            </ul>
        </div>
        <div class="col-md-5 col-sm-5">
            <div class="topHeaderBlock">
                <ul class="headerNav">
                    <li><a href="<?php the_field('link_page_1', 2); ?>" class="shop">Shop
                            cart</a></li>
                    <li>
                        <ul class="headerListSoc">
                            <li><div class="sendSearch"></div></li>
                                <?php
                                    $social = get_field('social', 2);
                                    if( $social ): ?>

                                        <li><a href="<?php echo $social['twiter']; ?>"><img src="<?php echo bloginfo ('template_url'); ?>/images/headerSoc/soc4.png" alt="twiter"></a></li>
                                        <li><a href="<?php echo $social['instagram']; ?>"><img src="<?php echo bloginfo ('template_url'); ?>/images/headerSoc/soc3.png" alt="instagram"></a></li>
                                        <li><a href="<?php echo $social['youtube']; ?>"><img src="<?php echo bloginfo ('template_url'); ?>/images/headerSoc/soc2.png" alt="youtube"></a></li>
                                        <li><a href="<?php echo $social['facebook']; ?>"><img src="<?php echo bloginfo ('template_url'); ?>/images/headerSoc/soc1.png" alt="facebook"></a></li>

                                     <?php endif; ?>
                        </ul>
                    </li>
                    <li><a href="<?php the_field('link_page_2', 2); ?>">donate</a></li>
                </ul>
                <?php the_field('header_text', 2); ?>
            </div>
            <div class="searchRow">
                <form role="search" method="get"  action="<?php echo esc_url(site_url('/')); ?>">

                    <div class="searchHeaderBlock">
                        <div class="small-8 columns">
                            <input type="text" value="<?php echo get_search_query() ?>" name="s"  placeholder="Search" class="inputSearch" autofocus/>
                        </div>
                        <div class="small-4 columns">
                            <input type="submit" value="Search" class="buttonSearch">
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
    <nav class="navbar navbar-default navbar-static-top">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed icon-menu menuActive" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div id="navbar" class="navbar-collapse menu">

            <ul class="nav navbar-nav ">
                <li class="dropdown">
                    <a href="<?php echo site_url('/blog') ?>" data-toggle="dropdown" class="dropdown-toggle">what‘s on</a>
                    <ul class="dropdown-menu">
                        <li><a href="<?php echo site_url('/uncategorized/chorus-oz-best-sing-ever') ?>">chorusoz 2017</a></li>
                        <li><a href="<?php echo site_url('/uncategorized/tudor-portraits') ?>">tudor portraits</a></li>
                        <li><a href="<?php echo site_url('/uncategorized/nordic-songs') ?>">nordic songs</a></li>
                        <li><a href="<?php echo site_url('/uncategorized/elgar-the-dream-of-gerontius') ?>">elgarthedreamofgerontius</a></li>
                        <li><a href="<?php echo site_url('/uncategorized/songs-of-farewell-music-of-rememberance') ?>">songs of farewell</a></li>
                        <li><a href="<?php echo site_url('/uncategorized/handel-messiah') ?>">handel messiah</a></li>
                    </ul>
                </li>
                <li><a href="<?php echo site_url('/support-us') ?>">support us</a></li>
                <li><a href="<?php echo site_url('/sing-with-us/workshop-discover-sing-with-us') ?>">sing with us</a></li>
                <li  class="dropdown">
                    <a href="#" data-toggle="dropdown" class="dropdown-toggle">about us</a>
                    <ul class="dropdown-menu">
                        <li><a href="#">music director</a></li>
                        <li><a href="#">history</a></li>
                        <li><a href="#">board and management</a></li>
                        <li><a href="#">chorister alumni</a></li>
                        <li><a href="#">contact</a></li>
                    </ul>
                </li>
                <li><a href="<?php echo site_url('/media/yuotube-video') ?>">media</a></li>
                <li><a href="<?php echo site_url('/contact') ?>">contact</a></li>
            </ul>
        </div>
    </nav>
</header>