
<div class="container margin-bottom">
	<div class="row ">
	
			<h2 class="titleArticle single-post-title"><?php the_title(); ?></h2>

			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				
				<div class="entry-content">
					<?php
					the_content();

					wp_link_pages(
						array(
							'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'philharmo' ),
							'after'  => '</div>',
						)
					);
					?>
				</div>

			</article>

		
	</div>	
</div>
